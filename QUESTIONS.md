# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

A1: It would output 2, then 1, as `setTimeout` method waits for 100ms to run the function in parameter.

Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

A2: It outputs from 10 to 0. The `foo(0)` is pending on the execution of `foo(1)`, which waits for `foo(2)` until `d < 10` which moves the next statement to console and give back the hand to calling function.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

A3: in case of `null`, `false`, `undefined`, `''`, `0` value set in parameter, we would run set `5` as response. We should evaluate the `argument` keyword instead.

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

A4: it outputs 3. `bar` is a function which hoists `a=1`.

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

A5: It would be used to delay the execution of `done` callback by 100ms, with a doubled value in input.