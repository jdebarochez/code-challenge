const request = require('supertest');
const server = require('./server');
const chai = require('chai');

const expect = chai.expect;

// this should be done in a test config
const port = 3000;

describe('GET /location', () => {
    let app;

    before(() => {
        app = server.listen(port);
    });

    it('returns a 200', function (done) {
        request(server)
            .get("/location")
            .expect(200, done);
    })

    it('returns an array of locations', function (done) {
        request(server)
            .get("/location")
            .end((err, res) => {
                expect(res.body).to.be.an('array');
                done();
            });
    })

    it('returns hastings when looking for it', function (done) {
        request(server)
            .get("/location?q=hastin")
            .end((err, res) => {
                expect(res.body).to.include('Hastings Castle');
                done();
            });
    })

    after(() => {
        app.close();
    })
})