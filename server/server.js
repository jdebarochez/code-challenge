const express = require('express');
const locationRepository = require('./locationRepository')

const app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/location', async (req, res) => {
    const data = await locationRepository.getAll(req.query.q);
    res.json(data.map(m => m.name))
})

module.exports = app;