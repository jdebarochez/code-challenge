const sqlite3 = require('sqlite3').verbose();

// this should be injected from the composition root index.js
// repository does not need to know where the database comes from
const db = new sqlite3.Database('./database/data.db');

module.exports = {
    getAll: (location = '') => {
        const filter = `%${location}%`;

        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM Locations WHERE (name LIKE ?) LIMIT 10", [filter], (err, rows) => {
                if (err) reject(err);

                resolve(rows);
            })
        });
    }
}