const server = require('./server');

// this should be in environment variables
const port = 3001;

server.listen(port, () => {
    console.log('Application listening on port: ' + port);
})