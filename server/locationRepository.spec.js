const locationRepository = require('./locationRepository.js');
const chai = require('chai');
const expect = chai.expect;

describe('LocationRepository', () => {
    it('load data from database (10 item max)', async () => {
        const data = await locationRepository.getAll();

        expect(data).to.be.an('array');
        expect(data.length).to.be.equal(10);
    })

    it('filters on location names', async () => {
        const data = await locationRepository.getAll("hasting");
        expect(data.map(m => m.name)).to.include('Hastings Castle');
    })
})