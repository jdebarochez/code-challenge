# Server side

## `npm run init`

Initialize the database with the tsv file

## `npm run start`

Run the server in development mode

## `npm run test:watch`

Run tests with a watcher for file changes
