import { render, screen } from '@testing-library/react';
import App from './App';

test('renders a search bar', () => {
  render(<App />);
  const linkElement = screen.getByRole("textbox");
  expect(linkElement).toBeInTheDocument();
});
