import LocationList from './locationlist'

function App() {
  return (
    <div className="App">
      <LocationList />
    </div>
  );
}

export default App;
