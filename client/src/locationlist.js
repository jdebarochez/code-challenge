const { Component } = require("react");

// to move to a config file eventually
const baseUrl = "http://localhost:3001";

// short hand for loading data with fetch
const fetcher = (...args) => fetch(...args).then(res => res.json())

// if the interaction with the API gets bigger, use a store to abstract calls from the components
const getData = async (query) => {
    return await fetcher(`${baseUrl}/location?q=${query}`);
}

// not handled yet:
//  - a message status to give user visual feedback
//  - restrict calls to 3 characters minimum to avoid irrelevant calls
//  - debounce the fetch calls to search after ~300ms once the user finishes their queries
export default class LocationList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            data: []
        }
    }

    render() {
        const data = this.state.data;

        return (
            <>
                <input type="text" onChange={this.handleChange.bind(this)} placeholder="Enter your query" />
                <ul>
                    {
                        data.map((d, i) => <li key={i}>{d}</li>)
                    }
                </ul>
            </>
        )
    }

    async handleChange(e) {
        const state = { ...this.state };

        state.data = await getData(e.target.value);

        this.setState(state);
    }
}